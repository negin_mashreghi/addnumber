# Project Description

Practicing automated deployments and testing on application that adds numbers using bitbucket pipeline.

---


# Clone and run node.js applications

This project involves three repositories.



### Frontend Repository

```

git clone https://negin_mashreghi@bitbucket.org/negin_mashreghi/addnumber_front.git

```
This following command will start a node express server and listens on port **3000** for connections. 

```
npm start

```
##### Run node app in background

nstallation
```
$ npm install forever 
```
Usage

```
$ forever start app.js

```

### Backend Repository

```
git clone https://negin_mashreghi@bitbucket.org/negin_mashreghi/addnumber.git

```
This following command will start a node express server and listens on port **8080** for connections. 

```
npm start

```
##### Run node app in background

nstallation
```
$ npm install forever 
```
Usage

```
$ forever start app.js

```


### Functional test Repository

```
git clone https://negin_mashreghi@bitbucket.org/negin_mashreghi/addnumber_functionaltest.git

```

---
# Bitbucket Pipelines

Bitbucket Pipelines is an integrated CI/CD service, built into Bitbucket. It allows you to automatically build, test and even deploy your code, based on a configuration file in your repository

##### Get started with Bitbucket Pipelines

1. Select Pipelines from the left hand navigation bar.
2. Select the language template you'd like to use. 

More info 

 - [Get started with Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/get-started-with-bitbucket-pipelines-792298921.html)
 - [Configure bitbucket-pipelines.yml](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html#Configurebitbucket-pipelines.yml-ci_services)
---
# Unit and Integration Tests 


In Backend Repository the test.js file contains three test cases 

- One **unit test**
    - Check if the addnumber function is working properly 
    
- Two **Integration test**
    - Testing /calculate endpoint to make sure that users are receiving correct static code base on the type of data their sending.
   
##### Tools and Frameworks

- [Mocha](https://mochajs.org/) is a feature-rich JavaScript test framework running on Node.js. 
- [Chai](https://www.chaijs.com/) is a BDD / TDD assertion library for node.



### Run test with bitbucket pipelines
Add the following code to bitbucket-pipelines.yml and bulid it. The result will be:

1. Create a docker container that use node:10.15.0 image and named it unit test
2. Run "npm test" and you will see the result of the unit test 

```
  
pipelines:  
  default:
    
    - step:
         name: unit test
         image: node:10.15.0 
         script:
           - npm test
```



---

# Auto Deployment

## Connect bitbucket pipeline to the server via SSH Access

Go to your terminal and follow the following steps.

[Use SSH keys in Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/use-ssh-keys-in-bitbucket-pipelines-847452940.html?_ga=2.58842091.777163107.1555597055-976080053.1542744993)


#### 1. Generate an SSH key in 

```
cd ~/.ssh
ssh-keygen -t rsa -b 4096 -N '' -f my_ssh_key

```
This command will generate 2 files.

- my_ssh_key
- my_ssh_key.pub

#### 2. Encode the private key

```
base64 < my_ssh_key

```

#### 3. Add the key as a secure environment variable
1. In the Bitbucket repository, choose Settings, then Environment variables
2. Copy the base64-encoded private key from the terminal.
3. Paste the encoded key as the value for an environment variable. Make sure to check Secured. and named it MY_SSH_KEY


#### 4. Add the public key to a remote host
If you have SSH access to the server, you can use the ssh-copy-id command. Typically, the command appends the key to the ~/.ssh/authorized_keys file on the remote host:
```
cd ~/.ssh
ssh-copy-id -i my_ssh_key username@hostname

```
Test the SSH access to the server:
```
ssh -i ~/.ssh/my_ssh_key username@hostname

```

#### 5. Create the my_known_hosts file and replace it to your repo

```
cd ~/.ssh
ssh-keyscan -t rsa hostname > my_known_hosts
cat my_known_hosts

```


Copy the content of my_known_hosts file and past it into the my_known_hosts file inside backend repository


#### 6. Tie everything together in the bitbucket-pipelines.yml file
Add the following code to bitbucket-pipelines.yml and bulid it. The result will be:

1. Create .ssh directory in root
2. Append the contents of ./my_known_hosts to ~/.ssh/known_hosts path.
3. Give permission to read the privet key, then decode it into ~/.ssh/id_rsa path.
4. Logging into the server via ssh


```
 - step:
        script:
           - mkdir -p ~/.ssh
           - cat my_known_hosts >> ~/.ssh/known_hosts
           - (umask  077 ; echo $MY_SSH_KEY | base64 --decode > ~/.ssh/id_rsa)
           - ssh $USER@$HOST 'echo "connected to `host` as $USER"'

```

## Use BitBucket's Pipelines to auto-deploy onto staging server


#### 1. clone the repository in staging server

#### 2. Add the following script to ./deploy.sh 

```
echo "Deploy script started"
# path to the clone repo in staging server
git pull origin master 
echo "Deploy script finished execution" 
```

#### 3. Update bitbucket-pipelines.yml 
Add the following code to bitbucket-pipelines.yml and bulid it. The result will be:

1. Create .ssh directory in root
2. Append the contents of ./my_known_hosts to ~/.ssh/known_hosts path.
3. Give permission to read the privet key, then decode it into ~/.ssh/id_rsa path.
4. Logging into the server via ssh and view the deploy.sh


```
 - step:
        name: deploy
        deployment: staging
        script:
           - mkdir -p ~/.ssh
           - cat my_known_hosts >> ~/.ssh/known_hosts
           - (umask  077 ; echo $MY_SSH_KEY | base64 --decode > ~/.ssh/id_rsa)
           - cat ./deploy.sh | ssh $USER@$HOST 
           - echo "Deploy step finished"

```

Another example [Using BitBucket Pipelines to Deploy onto server via SSH Access](https://stackoverflow.com/questions/50053687/using-bitbucket-pipelines-to-deploy-onto-vps-via-ssh-access)



---

# Functional test


#### Run functional test from another repository with Bitbucket Pipelines

Add the following code to bitbucket-pipelines.yml and bulid it. The result will be:

1. Clone the addnumber_functionaltest repository in root directory(/root)
2. Switch back to previous directory where we working earlier. (opt/atlassian/pipelines/agent/build)
    - Note: whan you bulid your pipeline your crrent repository will be clone in "opt/atlassian/pipelines/agent/build" path
3. Run the test.sh script file 
    - More info about the [addnumber_functionaltest](https://bitbucket.org/negin_mashreghi/addnumber_functionaltest.git)
 



```
 - step:
           name: functinal test 
           script:
            - cd ~
            - git clone https://yourUserName@bitbucket.org/yourUserName/addnumber_functionaltest.git functionaltest
            - cd -
            - sh ./test.sh

           services:
            - selenium
   
 
definitions:
   services:
     selenium:
       image: selenium/standalone-chrome:3.4.0
```


#### Services in Bitbucket Pipelines

In bitbucket-pipelines.yml you can define **services** and use it in steps. These services run in separate but linked containers. 

Read more about [use services in Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/use-services-and-databases-in-bitbucket-pipelines-874786688.html)

In order for the functional test to work, you need to add a service that use **selenium/standalone-chrom** image.
This selenium service will help to automate testing, using a web browser to perform tasks such as clicking, filling in fields and scrolling.

More info [Using Selenium-Docker Containers end-to-end testing](https://robotninja.com/blog/introduction-using-selenium-docker-containers-end-end-testing/)





