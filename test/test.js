var expect = require('chai').expect;
//var chai = require('chai');
var addTwoNumbers = require('../addTwoNumbers');
var server = require('../app.js')
//const request = require('supertest');
var chai = require('chai')
  , chaiHttp = require('chai-http');


chai.use(chaiHttp);


describe('addTwoNumbers()', function () {
  it('should add two numbers', function () {
    
    // 1. ARRANGE
    var x = 5;
    var y = 1;
    var sum1 = x + y;

    // 2. ACT
    var sum2 = addTwoNumbers(x, y);

    // 3. ASSERT
    expect(sum2).to.be.equal(sum1);

  });
});



/**
 * Testing post calculate endpoint
 * Intergrations test 
 */
 describe('POST /calculate currect input', function () {
  let data = {
      "num1": "6",
      "num2": "3",

  }
  it('respond with 200 created', function (done) {
    chai.request(server)
          .post('/calculate')
          .send(data)
          .set('Accept', 'application/json')
          .end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(200);
            done();      
         });
  });
});

describe('POST /calculate incurrect input', function () {
  let data = {
      "num1": "",
      "num2": "",

  }
  it('respond with 400 error', function (done) {
    chai.request(server)
          .post('/calculate')
          .send(data)
          .set('Accept', 'application/json')
          .end(function (err, res) {
            expect(err).to.be.null;
            expect(res).to.have.status(400);
            done();      
         });
  });
});
